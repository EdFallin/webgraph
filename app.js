let express = require("express");
let path = require("path");
let cookieParser = require("cookie-parser");
let sessioner = require("express-session");
let logger = require("morgan")("dev");

let favIconServer = require("serve-favicon");
let favIconPath = path.join(__dirname, "public", "images", "GraphFavIcon.png");

let indexRouter = require("./routes/index");
let graphRouter = require("./routes/graphs");
let testingRouter = require("./routes/testing");
let usersRouter = require("./routes/users");


// region session handling except app.use()

const sessionSecret = "A46539GHDYTT9BJKDBETTJFKJX256Y23432";
const sessionOptions = {
    secret: sessionSecret,
    cookie: {maxAge: 100000},
    resave: false,
    rolling: true,
    saveUninitialized: false,
    sameSite: "strict"
};

//* Used only to prevent unauthorized graph service calls. *//
let sessioning = (request, response, next) => {
    // when there's no user, redirect to the start page,
    // which calls to /isUser and displays welcome dialog
    if (!request.session.user) {
        response.status("404").send();
        return;
    }

    return next();
}

// endregion session handling except app.use()


let app = express();

app.use(logger);
app.use(sessioner(sessionOptions));

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, "public")));


app.use(favIconServer(favIconPath));

// service calls are routed through session handling so no
// unauthenticated / unauthorized service calls can be made
app.use("/graphs", sessioning);
app.use("/tests", sessioning);

app.use("/", indexRouter);

app.use("/graphs", graphRouter);
app.use("/tests", testingRouter);
app.use("/users", usersRouter);

module.exports = app;
