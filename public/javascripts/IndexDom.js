/**/

/* represents the DOM of index.html; a spoof need only have
   equivalent DOM elements or even shallow JS objects */

export class IndexDom {
    /* gets all elements needed in the DOM and aliases them;
       "dom" should be the browser's document object */
    constructor(dom) {
        this.dom = dom;

        this.welcomeBox = this.dom.getElementById("WelcomeBox");
        this.joinBox = this.dom.getElementById("JoinBox");
        this.dialogOverlay = this.dom.getElementById("DialogOverlay");
        this.identity = this.dom.getElementById("Identity");
        this.secret = this.dom.getElementById("Secret");
        this.visitor = this.dom.getElementById("Visitor");
        this.joinDialoger = this.dom.getElementById("JoinDialoger");
        this.identityStarter = this.dom.getElementById("IdentityStarter");
        this.secretStarter = this.dom.getElementById("SecretStarter");
        this.joiner = this.dom.getElementById("Joiner");
        this.leaver = this.dom.getElementById("Leaver");

        this.jobText = this.dom.getElementById("JobText");
        this.readier = this.dom.getElementById("Readier");
        this.reverser = this.dom.getElementById("Reverser");
        this.clearer = this.dom.getElementById("Clearer");

        this.jobLikelies = this.dom.getElementById("JobLikelies");

        this.finder = this.dom.getElementById("Finder");
        this.adder = this.dom.getElementById("Adder");
        this.stasher = this.dom.getElementById("Stasher");
        this.trasher = this.dom.getElementById("Trasher");

        this.found = this.dom.getElementById("Found");
        this.alreadies = this.dom.getElementById("Already");

        this.bubble = this.dom.getElementById("BubblePrototype");

        this.draggables = this.gatherDraggables();
        this.droppables = this.gatherDroppables();
    }

    gatherDraggables() {
        // retrieving one set of DOM els for now
        let draggables = this.dom.getElementsByClassName("draggable");

        // returning the set as a plain array
        draggables = Array.from(draggables);
        return draggables;
    }

    gatherDroppables() {
        // retrieving two sets of DOM els by one class each,
        // because putting both classes together only finds
        // the elements that have both classes together
        let droppables = Array.from(this.dom.getElementsByClassName("droppable"));
        let dropToActs = Array.from(this.dom.getElementsByClassName("drop-to-act"));

        // returning the sets joined, as a plain array
        droppables = droppables.concat(dropToActs);
        return droppables;
    }

    getElementById(id) {
        let element = this.dom.getElementById(id);
        return element;
    }

    getParentByChildId(id) {
        let child = this.dom.getElementById(id);
        let parent = child.parentElement;
        return parent;
    }
}
