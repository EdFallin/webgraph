/**/


import {TestKit} from "./TestKit.js";
import {BinarySearch} from "./BinarySearch.js";

export class BinarySearchTests {
    constructor() {
        this.kit = new TestKit();
    }

    define() {
        this.kit.group("BinarySearch", () => {
                // region searchExact()

                this.kit.test("searchExact",
                    "When the sought is present, its index is returned.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12];
                        let sought = 4;

                        let expected = 1;  // index of sought in subject

                        //* act *//
                        let actual = BinarySearch.searchExact(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                this.kit.test("searchExact",
                    "When the sought is not present, fail value of -1 is returned.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12];
                        let sought = 3;

                        let expected = -1;  // predefined fail value

                        //* act *//
                        let actual = BinarySearch.searchExact(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                this.kit.test("searchExact",
                    "When all present and skipped values are tried over an even length, "
                    + "correct index or -1 returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12];
                        let soughts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
                        let expecteds = [-1, 0, -1, 1, -1, 2, -1, 3, -1, 4, -1, 5];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchExact(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchExact",
                    "When all present and skipped values are tried over an odd length, "
                    + "correct index or -1 returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
                        let expecteds = [-1, 0, -1, 1, -1, 2, -1, 3, -1, 4, -1, 5, -1, 6];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchExact(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchExact",
                    "When a value above all in subject is tried, fail value of -1 is returned.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8];
                        let sought = 9
                        let expected = -1;

                        //* act *//
                        let actual = BinarySearch.searchExact(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                this.kit.test("searchExact",
                    "When the subject is empty, fail value of -1 is returned.",
                    () => {
                        //* arrange *//
                        let subject = [];
                        let sought = 6;
                        let expected = -1;

                        //* act *//
                        let actual = BinarySearch.searchExact(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                // endregion searchExact()

                // region searchInexactLower()

                this.kit.test("searchInexactLower",
                    "When all present values are tried, exact index returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [2, 4, 6, 8, 10, 12, 14];

                        let expecteds = [0, 1, 2, 3, 4, 5, 6];
                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactLower(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactLower",
                    "When all skipped values are tried, 1 lower returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [3, 5, 7, 9, 11, 13, 15];

                        let expecteds = [0, 1, 2, 3, 4, 5, 6];
                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactLower(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactLower",
                    "When values below all in subject are tried, fail value of -1 returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [0, -10];

                        let expecteds = [-1, -1];
                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactLower(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactLower",
                    "When values above all in subject are tried, top index returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [15, 19];

                        let expecteds = [6, 6];
                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactLower(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactLower",
                    "When the subject is empty, fail value of -1 is returned.",
                    () => {
                        //* arrange *//
                        let subject = [];
                        let sought = 6;
                        let expected = -1;

                        //* act *//
                        let actual = BinarySearch.searchInexactLower(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                // endregion searchInexactLower()

                // region searchInexactHigher()

                this.kit.test("searchInexactHigher",
                    "When all present values are tried, exact index returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [2, 4, 6, 8, 10, 12, 14];
                        let expecteds = [0, 1, 2, 3, 4, 5, 6];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactHigher(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactHigher",
                    "When all skipped values are tried, 1 greater returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [1, 3, 5, 7, 9, 11, 13];
                        let expecteds = [0, 1, 2, 3, 4, 5, 6];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactHigher(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactHigher",
                    "When values above all in subject are tried, fail value of -1 returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [15, 16];
                        let expecteds = [-1, -1];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactHigher(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactHigher",
                    "When values below all in subject are tried, least index of 0 returned for each.",
                    () => {
                        //* arrange *//
                        let subject = [2, 4, 6, 8, 10, 12, 14];
                        let soughts = [-2, 0];
                        let expecteds = [0, 0];

                        let actuals = [];

                        //* act *//
                        for (let sought of soughts) {
                            let actual = BinarySearch.searchInexactHigher(subject, sought);
                            actuals.push(actual);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            // stops at the first incorrect actual
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("searchInexactHigher",
                    "When the subject is empty, fail value of -1 is returned.",
                    () => {
                        //* arrange *//
                        let subject = [];
                        let sought = 6;
                        let expected = -1;

                        //* act *//
                        let actual = BinarySearch.searchInexactHigher(subject, sought);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                // endregion searchInexactHigher()
            }
        );
    }

    *run() {
        // creating the test list
        this.define();

        // actually running the tests
        yield* this.kit.run();
    }
}
