import bcrypt from "bcrypt";
import MongoClient from "mongodb";

const mongoIp = "127.0.0.1";
const mongoPort = 27017;
const mongoProtocol = "mongodb://";

const mongoUrl = `${mongoProtocol}${mongoIp}:${mongoPort}`;
const mongoOptions = {useUnifiedTopology: true};

const mongoStoreName = "local";
const collectionName = "users";

const rounds = 12;

const defaultIdentity = "friend";
const defaultSecret = "enter"

//* handles user identification for the site, not including their graphs *//
export class UserServer {
    // region construction

    constructor() {
        // defined here for discoverability
        this.mongoClient = null;
        this.store = null;
        this.users = null;
        this.salt = null;
    }

    async init() {
        await this.initHashing();
        await this.lazyInitUserStore();
        await this.lazyInitDefaultUser();
    }

    //* hashing is an integral part of handling stored user identities *//
    async initHashing() {
        this.salt = await bcrypt.genSalt(rounds);
    }

    async lazyInitUserStore() {
        // laziness
        if (this.mongoClient != null) {
            return;
        }

        // initing link
        this.mongoClient = await new MongoClient(
            mongoUrl, mongoOptions
        );

        // linking
        await this.mongoClient.connect();

        // initing the store itself
        this.store = this.mongoClient.db(mongoStoreName);

        // initing the target collection
        this.users = this.store.collection(collectionName);
    }

    async lazyInitDefaultUser() {
        // getting
        let user = await this.getUser(defaultIdentity);

        // laziness
        if (user) {
            return;
        }

        // actually initing, factored
        await this.addUser(defaultIdentity, defaultSecret);
    }

    // endregion construction


    // region destruction

    //* used to close down the MongoDb connection so that
    //  each method doesn't need to open and close it *//
    destroy() {
        this.mongoClient.close()
            .then(() => {
            });
    }

    // endregion destruction


    async addUser(identity, secret) {
        // readying
        let hashword = await bcrypt.hash(secret, this.salt);

        let user = {
            identity,
            secret: hashword
        }

        // actually adding
        let result = await this.users.save(user);

        return result;
    }

    /* returns the user with the matching identity, or `undefined` */
    async getUser(identity) {
        let sought = {identity};
        let any = await this.users.findOne(sought);
        return any;
    }

    async removeUser(identity) {
        let target = {identity};
        let result = await this.users.removeOne(target);

        return result;
    }

}

