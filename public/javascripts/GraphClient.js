/**/

export class GraphClient {
    constructor() {
    }

    async init() {
        //* future: possibly change this to use promise-then() syntax, if async causes problems *//
        try {
            let started = await fetch("/graphs/start");

            if (!started.ok) {
                throw new Error("Could not contact server.");
            }
        }
        catch (e) {
            // rethrowing because there's nothing useful to do here
            throw e;
        }
    }

    async topEightGraphItems() {
        try {
            let result = await fetch("/graphs/topEight");

            if (!result.ok) {
                throw new Error("Could not contact server.");
            }

            let topEight = await result.json();
            return topEight;
        }
        catch (e) {
            // rethrowing because there's nothing else useful to do here
            throw e;
        }
    }

    // region findInGraph() and dependencies

    async findInGraph(factors) {
        let factorsUriSegment = this.fromFactorsToUriSegment(factors);

        let founds = [];

        try {
            let result = await fetch(`/graphs/find/${factorsUriSegment}`);

            if (!result.ok) {
                //* future: make this more useful / specific *//
                throw new Error("Could not contact server.");
            }

            founds = await result.json();
        }
        catch (e) {
            //* future: make this more useful / specific *//
            // nothing more useful to do here right now
            throw (e);
        }

        return founds;
    }

    fromFactorsToUriSegment(factors) {
        let asJson = JSON.stringify(factors);
        let segment = encodeURIComponent(asJson);

        return segment;
    }

    // endregion findInGraph() and dependencies

    // region addToGraph() and dependencies

    async addToGraph(factors) {
        // arg must be changed to structured data
        let bundle = this.fromFactorsToBundle(factors);

        // JSON must always be sent as a string
        let body = JSON.stringify({bundle});

        try {
            // actually add to the graph
            let something = await fetch(
                "/graphs/add",
                {
                    method: "post",
                    headers: {"Content-Type": "application/json"},
                    body
                }
            );

            //* ?future: maybe more differentiated handling *//
            if (!something.ok) {
                throw new Error("Could not contact server.");
            }
        }
        catch (e) {
            // rethrowing because there's nothing useful to do here
            throw e;
        }
    }

    fromFactorsToBundle(factors) {
        let nodes = [];

        for (let factor of factors) {
            let node = {vertex: factor, edges: []}
            nodes.push(node);
        }

        let bundle = {nodes};
        return bundle;

        //* cruft: example data to send: *//
        // let bundle = {
        //     "nodes": [
        //         {"vertex": "a", "edges": []},
        //         {"vertex": "b", "edges": ["a"]},
        //         {"vertex": "c", "edges": ["a", "b"]}
        //     ]
        // };
    }

    // endregion addToGraph() and dependencies

    async removeFromGraph(factors) {
        let body = {factors};
        body = JSON.stringify(body);

        try {
            await fetch("/graphs/remove",
                {
                    method: "post",
                    headers: {"Content-Type": "application/json"},
                    body
                }
            );
        }
        catch (e) {
            throw e;
        }
    }

}
