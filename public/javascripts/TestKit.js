/**/

/* a class for testing that is simplified in that it doesn't require lots of secondary objects,
   and its methods are somewhat easier to call in individual test classes */

export class TestKit {
    constructor() {
        this.subject = null;
        this.tests = [];

        this.testRunPassed = true;
    }


    // region static constant properties

    /* these properties are defined so that they never change,
       in effect making them public constants, since otherwise
       they can't be used from the context of this class */

    static get TestGroup() {
        return "A";
    }

    static get PassedTest() {
        return "B";
    }

    static get FailedTest() {
        return "C";
    }

    static get NetTestResult() {
        return "D";
    }

    static get AllTestsRun() {
        return "E";
    }

    static get TestFailedText() {
        return "Test Failed.";
    }

    // endregion static constant properties


    // region test-definition methods

    group(subject, nonce) {
        // setting the group / subject (the class under test);
        // the nonce function is ignored, because the calls to
        // test() and equal() in it are what are actually used
        this.subject = subject;

        // clearing any existing test definitions,
        // so tests won't be repeated in one run
        this.tests = [];

        // running the nonce, which should contain calls
        // to test(), thereby setting up the tests to run
        nonce();
    }

    test(method, tested, nonce) {
        //* adds a test and info about it to the list to run *//
        let testDefinition = {method, tested, nonce};
        this.tests.push(testDefinition);
    }

    equate(expected, actual) {
        // a passed test is just a smooth exit from the test nonce;
        // testing for equivalence of values, not object identity
        if (expected == actual) {
            return;
        }

        // a failed test is a throw with certain standard attributes
        let thrown = new Error(TestKit.TestFailedText);
        thrown.expected = expected;
        thrown.actual = actual;

        throw thrown;
    }

    // endregion test-definition methods


    // region *run() and dependencies

    /* defining run() with a leading asterisk ( * ) like this
       makes it a generator, which returns one result each time
       a next() call is made on it like this: ...run.next() */
    * run() {
        // resetting default result state
        this.testRunPassed = true;

        // before any testing, the class under test is output
        yield this.reportClass();

        // each test is run in turn and its results output;
        // expecteds and actuals are handled by equal()
        for (let {method, tested, nonce} of this.tests) {
            yield this.runTest({method, tested, nonce});
        }

        // after all tests are run, the net result is output
        yield this.reportNetState();
    }

    reportClass() {
        // assembling output text
        let text = `${this.subject}`;

        return {type: TestKit.TestGroup, text: text};
    }

    runTest({method, tested, nonce}) {
        let result = null;

        try {
            // actually running the test
            nonce();

            // output
            let resulted = {didPass: true, thrown: null};
            result = this.reportTest({method, tested, resulted});
            return result;
        }
        catch (thrown) {
            // any failed test
            let resulted = {didPass: false, thrown: thrown};
            result = this.reportTest({method, tested, resulted});

            // changing result state
            this.testRunPassed = false;
        }

        // always runs, since no rethrow in the catch
        return result;
    }

    reportTest({method, tested, resulted: {didPass, thrown}}) {
        // assembling and standardizing output text
        let text = `${method}() : ${tested}`;
        text = text.replace("()()", "()");

        // default for each test, which is replaced as needed
        let testResult = TestKit.PassedTest;

        // when a test throws any exception, it's a failed test
        if (!didPass) {
            // regular fail has standard properties to output
            if (thrown.message == TestKit.TestFailedText) {
                text = this.textAtFailOnEquate(text, thrown);
            }
            // unexpected fail has throw passed along as received
            else {
                text = this.textAtFailOnThrow(text, thrown);
            }

            // whatever the fail reason, the test is reported as failed
            testResult = TestKit.FailedTest;
        }

        return {type: testResult, text: text};
    }

    textAtFailOnEquate(text, thrown) {
        text +=
            `
            Expected: ${thrown.expected}
            Actual: ${thrown.actual}
        `;
        return text;
    }

    textAtFailOnThrow(text, thrown) {
        text +=
            `
              Exception thrown:
                ${thrown.message}
                
              Stack trace:
                ${thrown.stack}
        `;
        return text;
    }

    reportNetState() {
        // differing return value for the result of
        // all tests has .didPass instead of .text
        return {
            type: TestKit.NetTestResult,
            didPass: this.testRunPassed
        };
    }

    // endregion *run() and dependencies
}
