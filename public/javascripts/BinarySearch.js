/**/

export class BinarySearch {
    //* returns the exact index of the sought if found, or -1 if it's not present in the subject *//
    static searchExact(subject, sought) {
        // bounds are outermost indices that actually exist
        let bottom = 0;
        let top = subject.length - 1;

        let repeats = 0;

        // continuing as long as probing can be done
        while (bottom <= top) {
            let midpoint = Math.floor((bottom + top) / 2);  // because JS doesn't have int-div
            let sample = subject[midpoint];

            // sought was found, so returning its index
            if (sample === sought) {
                return midpoint;
            }

            // if it's present, sought is higher than the sample,
            // so rebounding to the upper half of current range
            if (sample < sought) {
                bottom = midpoint + 1;
            }
            // if it's present, sought is lower than the sample,
            // so rebounding to the lower half of current range
            if (sample > sought) {
                top = midpoint - 1;
            }
        }

        // sought never found, so returning a fail value
        return -1;
    }

    /* if the sought is found, returns its exact index;
     * if the sought isn't found, returns the next lower index;
     * if the sought is higher than all in the subject, returns its top index;
     * if the sought is lower than all in the subject, returns -1 */
    static searchInexactLower(subject, sought) {
        // bounds are outermost indices that actually exist
        let bottom = 0;
        let top = subject.length - 1;

        // returned when the sought is lower than all values
        let nearest = -1;

        // continuing as long as probing can be done
        while (bottom <= top) {
            let midpoint = Math.floor((bottom + top) / 2);  // because JS doesn't have int-div
            let sample = subject[midpoint];

            // sought was found, so returning its index
            if (sample === sought) {
                return midpoint;
            }

            // if it's present, sought is higher than the sample,
            // so rebounding to the upper half of current range,
            // and retaining nearest for possible later output
            if (sample < sought) {
                bottom = midpoint + 1;
                nearest = midpoint;
            }

            // if it's present, sought is lower than the sample,
            // so rebounding to the lower half of current range
            if (sample > sought) {
                top = midpoint - 1;
            }
        }

        // sought never found, so returning the closest below
        return nearest;
    }

    /* if the sought is found, returns its exact index;
    * if the sought isn't found, returns the next higher index;
    * if the sought is higher than all in the subject, returns -1;
    * if the sought is lower than all in the subject, returns 0 */
    static searchInexactHigher(subject, sought) {
        // bounds are outermost indices that actually exist
        let bottom = 0;
        let top = subject.length - 1;

        // returned when the sought is higher than all values
        let nearest = -1;

        // continuing as long as probing can be done
        while (bottom <= top) {
            let midpoint = Math.floor((bottom + top) / 2);  // because JS doesn't have int-div
            let sample = subject[midpoint];

            // sought was found, so returning its index
            if (sample === sought) {
                return midpoint;
            }

            // if it's present, sought is higher than the sample,
            // so rebounding to the upper half of current range
            if (sample < sought) {
                bottom = midpoint + 1;
            }

            // if it's present, sought is lower than the sample,
            // so rebounding to the lower half of current range,
            // and retaining nearest for possible later output
            if (sample > sought) {
                nearest = midpoint;
                top = midpoint - 1;
            }
        }

        // sought never found, so returning the closest above
        return nearest;
    }

}
