/**/

// used to access enum-like constants on TestKit
import {TestKit} from "./TestKit.js";

export class TestRunController {
    // region construction

    constructor(dom) {
        this.dom = dom;
        TestRunController.HttpFailedText = "HTTP request to service failed.";
    }

    //* used to redirect to the main page when not authenticated *//
    async init() {
        let isUser = false;

        try {
            let result = await fetch("/users/isUser");

            if (!result.ok) {
                throw new Error("Could not get user state from server.");
            }

            isUser = await result.json();
        }
        catch (error) {
            isUser = false;
        }

        //* redirecting to the main page if not signed in as a user *//
        if (!isUser) {
            window.open("/", "_self");
        }
    }

    //* used to put together page state *//
    build() {
        this.buildReporters();
        this.wire();
    }

    buildReporters() {
        // using a Map to delegate different test
        // results to different output reporters
        let reporters = new Map(
            [
                [TestKit.TestGroup, this.reportClass],
                [TestKit.PassedTest, this.reportPassedTest],
                [TestKit.FailedTest, this.reportFailedTest],
                [TestKit.NetTestResult, this.gatherNetTestResult],
                [TestKit.AllTestsRun, this.reportNetResultAsBar]
            ]
        );

        // accessible throughout the class
        this.reporters = reporters;
    }

    wire() {
        this.dom.runButton.addEventListener("click", (e) => {
            this.runAllTests(e);
        });
    }

    // endregion construction


    // region running tests

    async runAllTests() {
        // resetting page and test state
        this.clearTestList();
        this.reportRunStartAsBar();
        this.havePassed = true;

        // initing testing state at the server, then iteratively
        // getting all results (group, test, and net results)
        try {
            let response = await fetch("/tests/start");

            // all problems are handled the same,
            // in one catch later in this method
            if (!response.ok) {
                throw Error(TestRunController.HttpFailedText)
            }

            let done = false;

            // iteratively getting test results of all
            // kinds until an all-run result is received
            while (!done) {
                let result = await this.getNextResult();

                // each result that comes back has an identifier that's
                // used to get the right output generator from the Map
                let reporter = this.reporters.get(result.type);

                // actually reporting the result
                reporter(result, this);

                // net test result is the last thing received or needed
                if (result.type == TestKit.AllTestsRun) {
                    done = true;
                }
            }
        }
        catch (x) {
            // all problems are handled the same in this catch
            this.reportFailedRun(x, this);
        }
    }

    async getNextResult() {
        let response = await fetch("/tests/next");

        if (response.ok) {
            let result = await response.json();
            return result;
        }
        else {
            // all problems are handled the same,
            // in one catch at the caller
            throw Error(TestRunController.HttpFailedText);
        }
    }

    // endregion running tests


    // region test reporting

    clearTestList() {
        this.dom.resultsDiv.innerHTML = null;
    }

    // although it's like a reporter, this is only run
    // directly by this class to reset the page's state
    reportRunStartAsBar() {
        /* making the top bar look all-new each time called
           (when tests are run anew), including removing
           any existing result-based appearance classes */

        this.dom.bar.innerText = "Tests not yet run.";
        this.dom.bar.classList.remove("any-failed", "all-passed");
        this.dom.bar.classList.add("none-yet-run");
    }

    // region reporters

    /* these reporters are found in a Map of them based on the
       TestKit-defined result type, then invoked as delegates;
       the 'self' arg passed to each of them is the 'this'
       of this class, which isn't in scope when they're run */

    reportClass(result, self) {
        // getting text, then refining it to make it header-ish
        let text = result.text;
        text += ":";

        // getting stencil for output, then outputting
        let blueStencil = self.dom.blueStencil;
        self.displayInDiv(blueStencil, text);
    }

    reportPassedTest(result, self) {
        // getting text, then refining it to make passing clear
        let text = result.text;
        text = "[+] : " + text;

        // getting stencil for output, then outputting
        let greenStencil = self.dom.greenStencil;
        self.displayInDiv(greenStencil, text);
    }

    reportFailedTest(result, self) {
        // getting text, then refining it to make fail clear
        let text = result.text;
        text = "[–] : " + text;

        // getting stencil for output, then outputting
        let redStencil = self.dom.redStencil;
        self.displayInDiv(redStencil, text);
    }

    reportFailedRun(thrown, self) {
        let text = null;

        if (thrown.message == TestRunController.HttpFailedText) {
            text = "The test service could not be reached.  "
                + "Exception returned: \r\n\r\n"
                + thrown;
        }
        else {
            text = thrown;
        }

        let redStencil = self.dom.redStencil;
        self.displayInDiv(redStencil, text);

        self.reportNetResultAsBar({didPass: false});
    }

    // although a reporter, this does not directly produce output,
    // but accumulates net results over many test classes
    gatherNetTestResult(result, self) {
        //* combining results: any fail means net result was a fail *//

        let didPass = result.didPass;
        self.havePassed = self.havePassed && didPass;
    }

    reportNetResultAsBar(result, self) {
        //* reporting either a net pass or a net fail *//

        if (self.havePassed) {
            self.dom.bar.innerText = "Test run passed.";
            self.dom.bar.classList.remove("any-failed");
            self.dom.bar.classList.add("all-passed");
        }
        else {
            self.dom.bar.innerText = "Test run failed.";
            self.dom.bar.classList.remove("all-passed");
            self.dom.bar.classList.add("any-failed");
        }
    }

    // endregion reporters

    // this is a dependency of most of the reporters,
    // used to actually produce the output display
    displayInDiv(stencil, text) {
        let bubble = stencil.cloneNode(true);
        bubble.textContent = text;

        this.dom.resultsDiv.appendChild(bubble);
    }

    // endregion test reporting
}
