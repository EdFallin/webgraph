/**/

export class TestRunDom {
    constructor(dom) {
        this.dom = dom;
        this.build();
    }

    build() {
        this.runButton = this.dom.getElementById("RunTestsButton");

        this.blueStencil = this.dom.getElementById("BlueStencil");
        this.greenStencil = this.dom.getElementById("GreenStencil");
        this.redStencil = this.dom.getElementById("RedStencil");

        this.resultsDiv = this.dom.getElementById("ResultsDiv");
        this.bar = this.dom.getElementById("NetResultBar");
    }
}
