/**/

import {BinarySearch} from "./BinarySearch.js";

export class BinaryInsert {
    static insert(subject, topic) {
        // initial degenerate case: empty subject doesn't need binary searching
        if (subject.length == 0) {
            subject.push(topic);
            return {subject, at: 0};
        }

        // getting the number to insert at; since the search returns a fail value
        // when there is no higher value, that is converted to an insertable index
        let insertAt = BinarySearch.searchInexactHigher(subject, topic);
        insertAt = insertAt == -1 ? subject.length : insertAt;

        // calling splice() with a number, 0, and value(s)
        // mutates the array, inserting at the numbered index
        subject.splice(insertAt, 0, topic);

        // output
        return {subject, at: insertAt};
    }
}
