/**/

import {TestKit} from "./TestKit.js";
import {BinaryDelete} from "./BinaryDelete.js";

export class BinaryDeleteTests {
    constructor() {
        this.kit = new TestKit();
    }

    define() {
        this.kit.group("BinaryDelete",
            () => {
                this.kit.test("delete",
                    "When the item to be deleted isn't present, the array is left unchanged.",
                    () => {
                        //* arrange *//
                        let actuals = [1, 2, 4, 5, 6, 7];  // no 3
                        let topic = 3;

                        let expecteds = [1, 2, 4, 5, 6, 7];

                        //* act *//
                        actuals = BinaryDelete.delete(actuals, topic);

                        //* assert *//
                        for (let of = 0; of < expecteds.length; of++) {
                            let expected = expecteds[of];
                            let actual = actuals[of];

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("delete",
                    "When the item to be deleted is present, the array is returned with it removed.",
                    () => {
                        //* arrange *//
                        let actuals = [1, 2, 3, 4, 5, 6, 7];
                        let topic = 6;

                        let expecteds = [1, 2, 3, 4, 5, 7];

                        //* act *//
                        actuals = BinaryDelete.delete(actuals, topic);

                        //* assert *//
                        // if nothing deleted, same length
                        this.kit.equate(expecteds.length, actuals.length);

                        // if nothing deleted, same contents
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("delete",
                    "When all items are deleted, an empty array is returned.",
                    () => {
                        //* arrange *//
                        let actuals = [1, 2, 3, 4, 5, 6, 7];
                        let topics = [3, 2, 6, 4, 5, 7, 1];  // out of order

                        let expecteds = [];

                        //* act *//
                        for (let topic of topics) {
                            actuals = BinaryDelete.delete(actuals, topic);
                        }

                        //* assert *//
                        // if everything deleted, zero-length
                        this.kit.equate(expecteds.length, actuals.length);
                    }
                );
            }
        );
    }

    *run() {
        // creating the tests, then running them
        this.define();
        yield* this.kit.run();
    }
}
