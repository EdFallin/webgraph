/**/

import {TestKit} from "./TestKit.js";
import {IndexController} from "./IndexController.js";

import {SpoofGraphClient} from "./Spoofs.js";
import {SpoofIndexDom} from "./Spoofs.js";

export class IndexControllerTests {
    constructor() {
        this.kit = new TestKit();
    }

    define() {
        this.kit.group(
            "IndexController",
            () => {
                // region bubbleDomFromArrayTree()

                this.kit.test("bubbleDomFromArrayTree",
                    "When called with a known array tree, returns the correct DOM-like bubble tree.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let tree = [
                            ["a", "b"],
                            ["c", "d", "e"]
                        ];

                        let parent = dom.alreadies;
                        parent.children = [];
                        parent.classList.add(topic.latest);

                        let styles = [topic.fact, topic.word];
                        let classes = ["gold", "gray"];

                        //* act *//
                        let actuals = topic.bubbleDomFromArrayTree(parent, tree, styles);

                        //* assert *//
                        for (let outer = 0; outer < tree.length; outer++) {
                            let expOuter = tree[outer]
                            let acOuter = actuals.children[outer];

                            this.kit.equate(expOuter.length, acOuter.children.length);
                            this.kit.equate(classes[0], acOuter.classList[0]);

                            for (let inner = 0; inner < expOuter.length; inner++) {
                                let expInner = expOuter[inner];
                                let acInner = acOuter.children[inner];

                                this.kit.equate(expInner, acInner.innerText);
                                this.kit.equate(classes[1], acInner.classList[0]);
                            }
                        }
                    }
                );

                this.kit.test("bubbleDomFromArrayTree",
                    "When called with a known array tree, returns bubble tree with correct unique IDs.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let parent = dom.alreadies;
                        parent.children = [];
                        parent.classList.add(topic.latest);
                        let styles = [topic.fact, topic.word];

                        let tree = [
                            ["a", "b"],
                            ["c", "d", "e"]
                        ];

                        // numbers are set as reached in depth-first preorder recursion,
                        // relative to the .guid when the bubble tree is begun;
                        // as IDs, they're all strings, but they're converted later
                        let first = topic.guid;
                        let expOuters = [first, first + 3];
                        let expInners = [first + 1, first + 2, first + 4, first + 5, first + 6];

                        //* act *//
                        let actuals = topic.bubbleDomFromArrayTree(parent, tree, styles);

                        //* assert *//
                        for (let outer = 0; outer < tree.length; outer++) {
                            // localizing, and converting the expected
                            // to a string for more exact equating
                            let expOuter = String(expOuters[outer]);
                            let acOuter = actuals.children[outer];

                            this.kit.equate(expOuter, acOuter.id);

                            for (let inner = 0; inner < expOuters.length; inner++) {
                                // removing from the inner expecteds gets the next needed value;
                                // I convert to a string for more exact equating
                                let expInner = String(expInners.shift());
                                let acInner = acOuter.children[inner].id;

                                this.kit.equate(expInner, acInner);
                            }
                        }
                    }
                );

                // endregion bubbleDomFromArrayTree()

                // region arrayTreeFromText()

                this.kit.test("arrayTreeFromText",
                    "When called with known multi-line text, returns as many second-level items in each line as words there.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = "first line\r\nthis is the second line\r\nthird line here";
                        let expecteds = [2, 5, 3];

                        //* act *//
                        let actuals = topic.arrayTreeFromText(arg);

                        //* assert *//
                        for (let of = 0; of < actuals.length; of++) {
                            let expected = expecteds[of];
                            let actual = actuals[of].length;

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("arrayTreeFromText",
                    "When called with known multi-line text separated by \\r\\n, returns as many top-level items as lines.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = "first line\r\nsecond line\r\nthird line";
                        let expected = 3;

                        //* act *//
                        let actual = topic.arrayTreeFromText(arg);

                        //* assert *//
                        this.kit.equate(expected, actual.length);
                    }
                );

                this.kit.test("arrayTreeFromText",
                    "When called with known multi-line text separated by \\n, returns as many top-level items as lines.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = "first line\nsecond line\nthird line";
                        let expected = 3;

                        //* act *//
                        let actual = topic.arrayTreeFromText(arg);

                        //* assert *//
                        this.kit.equate(expected, actual.length);
                    }
                );

                this.kit.test("arrayTreeFromText",
                    "When called with known single-line text, returns one top-level item.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = "first words second words third words";
                        let expected = 1;

                        //* act *//
                        let actual = topic.arrayTreeFromText(arg);

                        //* assert *//
                        this.kit.equate(expected, actual.length);
                    }
                );

                this.kit.test("arrayTreeFromText",
                    "When called with empty text, returns empty array.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = "";
                        let expected = 0;

                        //* act *//
                        let actual = topic.arrayTreeFromText(arg);

                        //* assert *//
                        this.kit.equate(expected, actual.length);
                    }
                );

                this.kit.test("arrayTreeFromText",
                    "When called with null text, returns empty array.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = null;
                        let expected = 0;

                        //* act *//
                        let actual = topic.arrayTreeFromText(arg);

                        //* assert *//
                        this.kit.equate(expected, actual.length);
                    }
                );

                // endregion arrayTreeFromText()

                // region appendBubbleClone()

                this.kit.test("appendBubbleClone",
                    "When called with known args, appends a bubble with the expected properties to the provided parent.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // empty node to append to
                        let parent = dom.alreadies;
                        parent.children = [];

                        let expText = "content";
                        let expColor = "red";

                        //* act *//
                        topic.appendBubbleClone(expText, topic.noMatch, parent);

                        //* assemble *//
                        let actual = parent.children[0];

                        //* assert *//
                        this.kit.equate(expText, actual.innerText);
                        this.kit.equate(expColor, actual.classList[0]);
                    }
                );

                this.kit.test("appendBubbleClone",
                    "When called with known args, returns a bubble with the expected properties.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // empty node to append to
                        let parent = dom.alreadies;
                        parent.children = [];

                        let expText = "content";
                        let expColor = "red";

                        //* act *//
                        let actual = topic.appendBubbleClone(expText, topic.noMatch, parent);

                        //* assert *//
                        this.kit.equate(expText, actual.innerText);
                        this.kit.equate(expColor, actual.classList[0]);
                    }
                );

                this.kit.test("appendBubbleClone",
                    "When called twice, returns bubbles with the expected sequential IDs.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // expecteds here since the first is also input
                        let expFirst = 7;
                        let expSecond = expFirst + 1;

                        // ensuring the IDs start where I want
                        topic.guid = expFirst;

                        // empty node to append to
                        let parent = dom.alreadies;
                        parent.children = [];

                        //* act *//
                        let acFirst = topic.appendBubbleClone("a", topic.noMatch, parent);
                        let acSecond = topic.appendBubbleClone("b", topic.noMatch, parent);

                        //* assert *//
                        this.kit.equate(expFirst, acFirst.id);
                        this.kit.equate(expSecond, acSecond.id);
                    }
                );

                // endregion appendBubbleClone()

                // region useFounds()

                this.kit.test("useFounds",
                    "When given an empty array indicating no match in graph, adds one no-match bubble with predefined text and class \"red\".",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // spoofing no matches
                        let found = [];

                        let expChildCount = 1;
                        let expText = topic.noMatch;
                        let expColor = "red";

                        //* act *//
                        topic.useFounds(found);

                        //* assemble *//
                        let actuals = dom.found.children;
                        let actual = actuals[0];

                        //* assert *//
                        // first medatada, to ensure no stray region contents
                        this.kit.equate(expChildCount, actuals.length);

                        // actual spoofs a bubble node, but classList is nontrivial
                        this.kit.equate(expText, actual.innerText);
                        this.kit.equate(expColor, actual.classList[0]);
                    }
                );

                this.kit.test("useFounds",
                    "When given an array of matched vertices in graph, adds child bubbles for each of them with class \"green\".",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let found = [{name:"first", edges: []}, {name:"two", edges: []}];

                        let expTexts = Array.from(found, x => x.name);
                        let expColor = "green";

                        //* act *//
                        topic.useFounds(found);

                        //* assemble *//
                        let actuals = dom.found.children;

                        //* assert *//
                        this.kit.equate(expTexts.length, actuals.length);

                        for (let at = 0; at < expTexts.length; at++) {
                            this.kit.equate(expTexts[at], actuals[at].innerText);
                            this.kit.equate(expColor, actuals[at].classList[0]);
                        }
                    }
                );

                this.kit.test("useFounds",
                    "When given an array of matched vertices in graph, any previous child bubbles are replaced.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // previous child to be replaced
                        let previous = "failed";
                        topic.appendBubbleClone(previous, topic.latest, dom.found);

                        let found = [{name: "first", edges: []}, {name: "two", edges: []}];

                        let expTexts = Array.from(found, x => x.name);
                        let expColor = "green";

                        //* pre-testing *//
                        this.kit.equate(1, dom.found.children.length);

                        let existing = dom.found.children[0];
                        this.kit.equate(previous, existing.innerText);
                        this.kit.equate("gold", existing.classList[0]);

                        //* act *//
                        topic.useFounds(found);

                        //* assemble *//
                        let actuals = dom.found.children;

                        //* assert *//
                        this.kit.equate(expTexts.length, actuals.length);

                        for (let at = 0; at < expTexts.length; at++) {
                            this.kit.equate(expTexts[at], actuals[at].innerText);
                            this.kit.equate(expColor, actuals[at].classList[0]);
                        }
                    }
                );

                // endregion useFounds()

                // region getColorByType()

                this.kit.test("getColorByType",
                    "When .top8 type, top-8 class \"darkblue\" returned.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        let arg = topic.top8;
                        let expected = "darkblue";

                        //* act *//
                        let actual = topic.getColorByType(arg);

                        //* assert *//
                        this.kit.equate(expected, actual);
                    });

                // endregion getColorByType()

                // region useAlreadies()

                this.kit.test("useAlreadies",
                    "When run, the expected 8 top items are added as child nodes.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // removing any children created at IndexController init
                        let target = dom.alreadies;
                        target.children = [];

                        //* act *//
                        topic.useAlreadies(state.topEight);

                        //* assert *//
                        this.kit.equate(8, target.children.length);
                    }
                );

                this.kit.test("useAlreadies",
                    "When run but there are no top 8 items, the default content is set.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // changing the spoof, which is invoked
                        // by fillAlreadies(), to return no items
                        state.topEight = [];

                        // adding some existing content to find, and its lookup
                        // value used within IndexController, which must match
                        let fake = "Spoof text.";
                        dom.alreadies.innerHTML = "inner HTML";
                        dom.alreadies.innerText = "inner text";
                        topic.defaultAlreadiesText = fake;

                        // these must be copied, or else they would be live
                        // and auto-mutated, so they'd always match actual
                        let expHtml = null;
                        let expText = fake;

                        //* act *//
                        topic.useAlreadies(state.topEight);

                        //* assemble *//
                        let acHtml = dom.alreadies.innerHTML;
                        let acText = dom.alreadies.innerText;

                        //* assert *//
                        this.kit.equate(expHtml, acHtml);
                        this.kit.equate(expText, acText);
                    }
                );

                this.kit.test("useAlreadies",
                    "When run, the expected 8 top items all have the correct \"darkblue\" styling class.",
                    () => {
                        //* arrange *//
                        let dom = new SpoofIndexDom();
                        let state = new SpoofGraphClient();
                        let topic = new IndexController(dom, state);

                        // removing any children created at IndexController init
                        let target = dom.alreadies;
                        target.children = [];

                        let expected = "darkblue";

                        //* act *//
                        topic.useAlreadies(state.topEight);

                        //* assemble *//
                        let actuals = target.children;

                        //* assert *//
                        for (let of = 0; of < 8; of++) {
                            // targeted value is the only / first "class"
                            let actual = actuals[of];
                            actual = actual.classList[0];

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                // endregion useAlreadies()
            }
        );
    }

    * run() {
        this.define();
        yield* this.kit.run();
    }
}
