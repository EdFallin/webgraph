/**/

import {TestKit} from "./TestKit.js";
import {BinaryInsert} from "./BinaryInsert.js";

export class BinaryInsertTests {
    constructor() {
        this.kit = new TestKit();
    }

    define() {
        this.kit.group("BinaryInsert", () => {
                this.kit.test("insert",
                    "When the subject is empty, the new value becomes its first element.",
                    () => {
                        //* arrange *//
                        let subject = [];
                        let topic = "a";

                        let expLength = 1;
                        let expected = "a";

                        //* act *//
                        // subject arg is mutated, so return value isn't needed
                        BinaryInsert.insert(subject, topic);

                        //* assemble *//
                        let acLength = subject.length;
                        let acItem = subject[0];

                        //* assert *//
                        this.kit.equate(expLength, acLength);
                        this.kit.equate(expected, acItem);
                    }
                );

                this.kit.test("insert",
                    "When a single new value is added, it is placed in sorted order.",
                    () => {
                        //* arrange *//
                        let subject = ["a", "c", "d"];
                        let topic = "b";

                        let expecteds = ["a", "b", "c", "d"];

                        //* act *//
                        // subject arg is mutated, so return value isn't needed
                        BinaryInsert.insert(subject, topic);

                        //* assemble *//
                        let actuals = subject;

                        //* assert *//
                        this.kit.equate(expecteds.length, actuals.length);

                        for (let of = 0; of < expecteds.length; of++) {
                            let expected = expecteds[of];
                            let actual = subject[of];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("insert",
                    "When all values are added out of order, they are placed in sorted order.",
                    () => {
                        //* arrange *//
                        let topics = ["b", "q", "t", "a", "c", "n", "r", "f"];

                        let expecteds = ["a", "b", "c", "f", "n", "q", "r", "t"];
                        let actuals = [];

                        //* act *//
                        for (let topic of topics) {
                            // return values not used; actuals is mutated
                            BinaryInsert.insert(actuals, topic);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("insert",
                    "When numeric values are added out of order, they are placed in numerically sorted order.",
                    () => {
                        //* arrange *//
                        let topics = [1, 3, 11, 2, 33, 4, 6, 5, 8, 77, 7];

                        let expecteds = [1, 2, 3, 4, 5, 6, 7, 8, 11, 33, 77];
                        let actuals = [];

                        //* act *//
                        for (let topic of topics) {
                            // return values not used; actuals is mutated
                            BinaryInsert.insert(actuals, topic);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("insert",
                    "When a new value is added, the index returned for it matches its actual index.",
                    () => {
                        //* arrange *//
                        let original = []
                        let topics = ["a", "c", "f", "d", "b", "e"];

                        // these are the insert-at indices for the state
                        // of the array at the time they're inserted
                        let expecteds = [0, 1, 2, 2, 1, 4];
                        let actuals = [];

                        //* act *//
                        for (let topic of topics) {
                            // only one of the two returned members is used
                            let {at} = BinaryInsert.insert(original, topic);
                            actuals.push(at);
                        }

                        //* assert *//
                        for (let of = 0; of < expecteds.length; of++) {
                            let expected = expecteds[of];
                            let actual = actuals[of];
                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("insert",
                    "When a new value is added, the array returned matches the mutated input array.",
                    () => {
                        //* arrange *//
                        let original = []
                        let topics = ["a", "c", "f", "d", "b", "e"];

                        // these are the insert-at indices for the state
                        // of the array at the time they're inserted
                        let expecteds = [
                            ["a"],
                            ["a", "c"],
                            ["a", "c", "f"],
                            ["a", "c", "d", "f"],
                            ["a", "b", "c", "d", "f"],
                            ["a", "b", "c", "d", "e", "f"]
                        ];
                        let actuals = [];

                        //* act *//
                        for (let topic of topics) {
                            // only one of the two returned members is used;
                            // it must be copied so each isn't later mutated
                            let {subject} = BinaryInsert.insert(original, topic);
                            actuals.push(Array.from(subject));
                        }

                        //* assert *//
                        // comparing arrays of arrays
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            this.kit.equate(expected.length, actual.length);

                            for (let of = 0; of < expected.length; of++) {
                                this.kit.equate(expected[of], actual[of]);
                            }
                        }
                    }
                );
            }
        );
    }

    * run() {
        // creating the tests, then running them
        this.define();
        yield* this.kit.run();
    }
}
