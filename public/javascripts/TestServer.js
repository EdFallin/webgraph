/**/

/* TestServer provides test-running capabilities to the RESTful
    testing service defined in its own HTTP routing file. */

import {TestKit} from "./TestKit.js";

import {BinarySearchTests} from "./BinarySearchTests.js";
import {BinaryInsertTests} from "./BinaryInsertTests.js";
import {BinaryDeleteTests} from "./BinaryDeleteTests.js";
import {GraphTests} from "./GraphTests.js";
import {GraphServerTests} from "./GraphServerTests.js";
import {GraphClientTests} from "./GraphClientTests.js";
import {IndexControllerTests} from "./IndexControllerTests.js";

/* All tests to be run are created as new instances
   of their classes in an array in defineAllTests(). */


export class TestServer {
    // region construction
    constructor() {
        //* no operations *//
    }

    start() {
        this.defineAllTests();

        // using next(), so getting *run() as object
        this.runner = this.run();
    }

    defineAllTests() {
        let testSets = [
            new GraphTests(),
            new GraphClientTests(),
            new GraphServerTests() ,
            new IndexControllerTests(),
            new BinarySearchTests(),
            new BinaryInsertTests(),
            new BinaryDeleteTests()
        ];

        this.testSets = testSets;
    }

    // endregion construction


    // region running tests

    next() {
        let result = this.runner.next();
        return result.value;
    }

    /* defining run() with a leading asterisk ( * ) like this
       makes it a generator, which returns one result each time
       a next() call is made on it like this: ...run.next() */
    * run() {
        // running all tests provided to this test runner
        for (let testSet of this.testSets) {
            yield* testSet.run();
        }

        // final yield doesn't need an asterisk,
        // since it has no chained yields
        yield {type: TestKit.AllTestsRun};
    }

    // endregion running tests
}
