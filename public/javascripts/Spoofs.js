/**/

/* everything used by IndexController is emulated here,
   always synchronous rather than async so it's testable */
export class SpoofGraphClient {
    constructor() {
        this.doFindInGraph = false;

        // defined here as a property to allow changing for certain tests
        this.topEight = [
            "Jobs", "Companies", "Cities", "Recruiters",
            "Skills", "Liked", "Disliked", "Applied"
        ];

        this.toAdd = [];
    }

    topEightGraphItems() {
        //* a service call in real life *//
        return this.topEight;
    }

    findInGraph(factors) {
        if (!this.doFindInGraph) {
            return [];
        }
    }

    addToGraph(factors) {
        // splitting factors to individual args for multiple push()
        this.toAdd.push(...factors);
    }

    removeFromGraph(factors) {
        //* no implementation right now *//
    }
}

/* everything used in the DOM is emulated here trivially,
   or as trivially as I can manage for its use */
export class SpoofIndexDom {
    constructor() {
        this.welcomeBox = new SpoofNode();
        this.joinBox = new SpoofNode();
        this.dialogOverlay = new SpoofNode();
        this.identity = new SpoofNode();
        this.secret = new SpoofNode();
        this.visitor = new SpoofNode();
        this.joinDialoger = new SpoofNode();
        this.identityStarter = new SpoofNode();
        this.secretStarter = new SpoofNode();
        this.joiner = new SpoofNode();
        this.leaver = new SpoofNode();

        this.jobText = new SpoofNode();
        this.readier = new SpoofNode();
        this.reverser = new SpoofNode();
        this.clearer = new SpoofNode();

        this.jobLikelies = new SpoofNode();

        this.finder = new SpoofNode();
        this.adder = new SpoofNode();
        this.stasher = new SpoofNode();
        this.trasher = new SpoofNode();

        this.found = new SpoofNode();
        this.alreadies = new SpoofNode();

        this.bubble = new SpoofBubble();

        this.draggables = [];
        this.droppables = [];
        this.trasher = new SpoofNode();
    }
}


/* these classes are used in SpoofIndexDom
   to make syntax simpler and more reliable */

class SpoofNode {
    constructor() {
        this.innerText = null;
        this.html = null;
        this.value = null;

        this.classList = [];
        this.classList.add = (item) => {
            this.classList.push(item);
        };

        this.children = [];
        this.listeners = [];
    }

    // region .innerHTML

    // this property getter and setter are used to partly
    // imitate real el handling of a node's DOM subtree

    get innerHTML() {
        return this.html;
    }

    set innerHTML(html) {
        this.html = html;

        if (html == null) {
            for (let of = this.children.length -1; of >= 0; of--) {
                this.children.pop();
            }
        }
    }

    // endregion .innerHTML

    appendChild(child) {
        this.children.push(child);
    }

    addEventListener(listener) {
        this.listeners.push(listener);
    }
}

class SpoofBubble extends SpoofNode {
    constructor() {
        super();
        this.hidden = null;
    }

    cloneNode() {
        let clone = new SpoofBubble();

        clone.hidden = this.hidden;
        clone.innerText = this.innerText;
        clone.inner = this.inner;

        return clone;
    }
}

