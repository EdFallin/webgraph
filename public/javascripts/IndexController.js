/**/

//* defining class to be used as a controller; initing comes after *//
export class IndexController {
    // region Constructor and dependencies

    //* "dom" should be an IndexDom or equivalent *//
    constructor(dom, state) {
        //* future, factor this *//

        this.dom = dom;
        this.state = state;
        this.fact = "fact";
        this.word = "word";
        this.top8 = "top8already";
        this.latest = "latest";
        this.found = "found";
        this.trashed = "trashed";
        this.noMatch = "No match in graph.";

        this.imageTag = "IMG";
        this.defaultAlreadiesText = "Already in graph\r\n\r\n";

        this.bubbleColors = [
            {type: this.top8, class: "darkblue"},
            {type: this.latest, class: "gold"},
            {type: this.found, class: "green"},
            {type: this.noMatch, class: "red"},
            {type: this.fact, class: "gold"},
            {type: this.word, class: "gray"},
            {type: this.trashed, class: "red"}
        ];

        // IDs used for many bubbles, which must be unique across the page
        this.guid = 0;

        this.wireEventHandlers();
    }

    wireEventHandlers() {
        //* future, factor this *//

        // region site entry and exit

        // invoked when user hits enter on welcome dialog box
        this.dom.welcomeBox.addEventListener("keydown", (e) => {
            if (e.key === "Enter") {
                this.authenticateAndInitState();
            }
        });

        // invoked when user clicks Visit button on welcome dialog box
        this.dom.visitor.addEventListener("click", (e) => {
            this.authenticateAndInitState();
        });

        this.dom.joinDialoger.addEventListener("click", (e) => {
            this.promptToJoin();
        });

        this.dom.joiner.addEventListener("click", (e) => {
            this.addUser();
            this.initUsingState();
        });

        this.dom.leaver.addEventListener("click", (e) => {
            this.leave();
        });

        // endregion site entry and exit

        // region left side

        this.dom.jobText.addEventListener("input", (e) => {
            this.toggleStateOnJobText(e);
        });

        this.dom.readier.addEventListener("click", (e) => {
            this.convertJobTextToBubbles(e);
        });

        this.dom.reverser.addEventListener("click", (e) => {
            this.reverseFromBubblesToJobText(e);
        });

        this.dom.clearer.addEventListener("click", (e) => {
            this.clearJobText(e);
        });

        // delegated handling of this region when it is dropped elsewhere (onto),
        // and when an old branch of this region is dropped back onto it (into)
        this.dom.jobLikelies.atDropOnto = this.atDropJobLikeliesOnto;
        this.dom.jobLikelies.atDropInto = this.atDropIntoJobLikelies;

        // endregion left side

        // region generic drag and drop

        for (let draggable of this.dom.draggables) {
            draggable.addEventListener("dragstart", (e) => {
                this.whenDragged(e);
            });
        }

        for (let droppable of this.dom.droppables) {
            droppable.addEventListener("dragover", (e) => {
                this.whenOvered(e);
            });
            droppable.addEventListener("drop", (e) => {
                this.whenDropped(e);
            });
        }

        // endregion generic drag and drop

        // region custom handling of drops onto actors in middle

        let actors = [
            this.dom.finder,
            this.dom.adder,
            this.dom.stasher,
            this.dom.trasher
        ];

        for (let actor of actors) {
            actor.atDropInto = this.atDropIntoActor;
        }

        this.dom.finder.addEventListener("drop", (e) => {
            this.findInGraph(e);
        });

        this.dom.adder.addEventListener("drop", (e) => {
            this.addToGraph(e);
        });

        // endregion custom handling of drops onto actors in middle

        // region custom handling of drops onto the trasher actor

        this.dom.trasher.addEventListener("dragover", (e) => {
            this.whenTrashOvered(e);
        });

        this.dom.trasher.addEventListener("drop", (e) => {
            this.whenTrashed(e);
        });

        // endregion custom handling of drops onto the trasher actor
    }

    // endregion Constructor and dependencies


    // region Initializing state

    //* called by page script after construction *//
    initState() {
        // whether user is logged in or not
        this.getUserState()
            .then(
                () => {
                    this.initUxAndGraphState();
                }
            );
    }

    async getUserState() {
        this.isUser = false;

        try {
            let response = await fetch("/users/isUser");

            if (!response.ok) {
                throw new Error("Could not get user state from server.");
            }

            let isUser = await response.json();
            this.isUser = isUser;
        }
        catch (error) {
            this.isUser = false;
        }
    }

    initUxAndGraphState() {
        if (this.isUser) {
            this.clearWelcomingJoining();
            this.initUserUx();
        }
        else {
            this.initNonUserUx();
        }
    }

    clearWelcomingJoining() {
        this.dom.identity.value = null;
        this.dom.secret.value = null;
        this.dom.identityStarter.value = null;
        this.dom.secretStarter.value = null;
    }

    initUserUx() {
        // UX state
        this.dom.dialogOverlay.hidden = true;
        this.dom.welcomeBox.hidden = true;
        this.dom.joinBox.hidden = true;

        // graph state
        this.state.init();
        this.fillAlreadies();
    }

    initNonUserUx() {
        // UX state: prompt to visit or join
        this.dom.dialogOverlay.hidden = false;
        this.dom.welcomeBox.hidden = false;
        this.dom.joinBox.hidden = true;

        // make the identity input ready to be typed
        this.dom.identity.focus();

        //* no graph state, since no user *//
    }

    async fillAlreadies() {
        let alreadies = await this.state.topEightGraphItems();
        this.useAlreadies(alreadies);
    }

    /* private-style method is used to make handling of alreadies / top eight testable,
       without the complications of asynchronous testing */
    useAlreadies(alreadies) {
        // use the default content if there are no graph items
        if (alreadies.length == 0) {
            this.dom.alreadies.innerHTML = null;
            this.dom.alreadies.innerText = this.defaultAlreadiesText;
            return;
        }

        //* main path: dump any existing content and replace with the alreadies *//
        this.dom.alreadies.innerHTML = null;

        for (let already of alreadies) {
            this.appendBubbleClone(already, this.top8, this.dom.alreadies);
        }
    }

    // endregion Initializing state


    // region Site entry and exit

    authenticateAndInitState() {
        this.authenticateUser()
            .then(() => {
                this.initUxAndGraphState();
            });
    }

    async authenticateUser() {
        /* future: implement this for real security */
        try {
            let bundle = {
                identity: this.dom.identity.value,
                secret: this.dom.secret.value
            }

            bundle = JSON.stringify(bundle);

            // first fetch+ argument is the URL;
            // second arg is the method, headers, and body
            let response = await fetch(
                "/users/authenticateUser",
                {
                    method: "post",
                    headers: {"Content-Type": "application/json"},
                    body: bundle
                }
            );

            if (!response.ok) {
                throw new Error("Could not get user state from server.");
            }

            let isUser = await response.json();
            this.isUser = isUser;
        }
        catch (error) {
            this.isUser = false;
        }
    }

    addUser() {
        /* future: implement this */
    }

    promptToJoin() {
        this.dom.welcomeBox.hidden = true;
        this.dom.joinBox.hidden = false;
    }

    async leave() {
        /* future: probably drop or simplify this */
        let content = {content: "not needed"};
        content = JSON.stringify(content);

        try {
            let result = await fetch(
                "/users/leave",
                {
                    method: "post",
                    headers: {"Content-Type": "application/json"},
                    body: content
                });

            if (!result.ok) {
                throw new Error("Could not change user state on server.");
            }

            // logging out succeeded, so state is changed
            this.isUser = false;
        }
        catch (error) {
            // logging out failed, so .isUser is still true
            this.isUser = true;
        }

        // wiping the UX state with a reload
        if (!this.isUser) {
            window.open("/", "_self");
        }
    }

    // endregion Site entry and exit


    // region L side

    toggleStateOnJobText(e) {
        if (this.dom.jobText.value) {
            this.dom.clearer.disabled = false;
            this.dom.clearer.classList.remove("disabled");
        }
        else {
            this.dom.clearer.disabled = true;
            this.dom.clearer.classList.add("disabled");
        }
    }

    convertJobTextToBubbles(e) {
        let sender = e.currentTarget;
        let text = this.dom.jobText.value;
        let bubbleTree = this.arrayTreeFromText(text);

        let likelies = this.dom.jobLikelies;
        likelies.innerHTML = null;
        let styles = [this.fact, this.word];

        this.bubbleDomFromArrayTree(likelies, bubbleTree, styles);

        // hiding the input, while displaying
        // the parsed bubbles and the reverser
        this.dom.jobText.hidden = true;
        this.dom.readier.hidden = true;

        this.dom.jobLikelies.hidden = false;
        this.dom.reverser.hidden = false;

        // hiding the clearer while text hidden
        this.dom.clearer.hidden = true;
    }

    reverseFromBubblesToJobText(e) {
        // hiding the parsed bubbles,
        // while displaying the input
        this.dom.jobText.hidden = false;
        this.dom.readier.hidden = false;

        this.dom.jobLikelies.hidden = true;
        this.dom.reverser.hidden = true;

        // unhiding the clearer, and ensuring
        // its state matches text state
        this.dom.clearer.hidden = false;
        this.toggleStateOnJobText(e);
    }

    clearJobText(e) {
        // both clear text and refresh button's state
        this.dom.jobText.value = null;
        this.toggleStateOnJobText(e);
    }

    bubbleDomFromArrayTree(parent, tree, styles) {
        /* recursively, for each level, bubbles are made, with that level's style applied,
           and added to the preceding level (to the parent, at the first level) */

        // starting recursion at the root
        this.bubbleDomRecursor(parent, tree, styles, 0);

        // the root of the bubble DOM tree is returned
        return parent;
    }

    bubbleDomRecursor(parent, tree, styles, depth) {
        // for each item in root, add a bubble, and call this to add its child bubbles;
        // starts with children of root, not root itself, because that's just a container
        // for the top-level children, intended to be added directly to parent
        for (let child of tree) {
            // whenever the child is a text element, use it as the bubble's text
            let text = !Array.isArray(child) ? child : null;

            // add a bubble whether there's text or not, since enclosing bubbles have no text
            let next = this.appendBubbleClone(text, styles[depth], parent);

            // if the child isn't text, it's an array, so continue recursion
            if (Array.isArray(child)) {
                this.bubbleDomRecursor(next, child, styles, depth + 1);
            }
        }
    }

    arrayTreeFromText(text) {
        // fail path: no text, no bubbles;
        // handles both nulls and empties
        if (!text) {
            return [];
        }

        // main path:
        // first, splitting by any lines, then, by words in the lines
        let lines = text.split(/\r?\n/);

        for (let at = 0; at < lines.length; at++) {
            let words = lines[at].split(" ");
            lines[at] = words;
        }

        return lines;
    }

    // endregion L side


    // region Middle

    async findInGraph(e) {
        let id = e.dataTransfer.getData("text/plain");
        let dropped = this.dom.getElementById(id);
        let sought = dropped.innerText;

        let found = await this.state.findInGraph([sought]);
        this.useFounds(found);
    }

    useFounds(found) {
        // no text, and no child contents
        this.dom.found.innerText = null;
        this.dom.found.innerHTML = null;

        // if nothing was found, a default not-found bubble
        if (found.length == 0) {
            this.appendBubbleClone(this.noMatch, this.noMatch, this.dom.found);
        }

        // if anything was found, displaying it in bubbles
        for (let find of found) {
            this.appendBubbleClone(find.name, this.found, this.dom.found);
        }
    }

    async addToGraph(e) {
        //* future: probably change this to use the dragged values directly, possibly factored *//

        /* converts the items dropped on the adder to args for state . addToGraph();
            by the time this is called, they should already be children of the adder */

        let adder = this.dom.adder;

        // the adder contains one image, and the things to add
        let toAdd = Array.from(adder.children)
            .filter(x => x.tag != this.imageTag)
            .filter(x => x.innerText)
            .map(x => x.innerText);

        // to the service and then the server
        await this.state.addToGraph(toAdd);

        // updating the UI with the added factors
        await this.fillAlreadies();

        // dropping things from the adder after a short pause,
        // with some special-effect styling,
        // now that they've been added to the graph
        for (let child of this.dom.adder.children) {
            child.classList.add("faded");
        }

        setTimeout(() => this.dom.adder.innerHTML = null, 500);
    }

    // endregion Middle


    // region Duplicating elements

    appendBubbleClone(text, type, parent) {
        // creating the clone of the stencil
        let bubble = this.dom.bubble.cloneNode(true);
        bubble.innerText = text;

        // give each bubble an ID that's unique on this page;
        // postfix incrementing happens after value used here
        bubble.id = String(this.guid++);

        // styling the clone
        let color = this.getColorByType(type);
        bubble.classList.add(color);

        // all bubbles are draggable and droppable for now,
        // although this doesn't make sense in all cases
        this.makeElementDragAndDrop(bubble);

        // actually appending the clone
        parent.appendChild(bubble);

        // returning clone for building structures
        return bubble;
    }

    makeElementDragAndDrop(bubble) {
        // make the bubble listen to drag and drop events
        bubble.addEventListener("dragstart", (e) => {
            this.whenDragged(e);
        });
        bubble.addEventListener("dragover", (e) => {
            this.whenOvered(e);
        });
        bubble.addEventListener("drop", (e) => {
            this.whenDropped(e);
        });
    }

    getColorByType(type) {
        let item = this.bubbleColors
            .find((x) => x.type == type);

        let color = item.class;
        return color;
    }

    // endregion Duplicating elements


    // region Drag and drop, including custom delegates

    whenDragged(e) {
        // allowing nested drag and drop
        e.stopPropagation();

        // take any custom steps defined on the draggee
        let dragged = e.target;

        if (dragged.atDrag) {
            dragged.atDrag(e);
        }

        // save the identity of the draggee for dropping
        let id = e.target.id;  // the draggee
        e.dataTransfer.setData("text/plain", id);

        e.dataTransfer.dropEffect = "move";
    }

    atDragFromFinder(e) {
        let dragged = e.target;
        dragged.classList.remove("dropped-on-finder");
    }

    whenOvered(e) {
        // required for dnd to work
        e.preventDefault();
    }

    whenDropped(e) {
        // required for dnd to work
        e.preventDefault();

        // into = the drop zone, onto = the exact el dropped on
        let into = e.currentTarget;
        let onto = e.target;

        // if the el dropped on is the drop zone, insert at end with null,
        // but otherwise, insert before el dropped on
        let before = into !== onto ? onto : null;

        // since moving, not copying, we don't clone here
        let id = e.dataTransfer.getData("text/plain");
        let dragged = this.dom.getElementById(id);

        // take any custom steps defined on the drop zone
        if (into.atDropInto) {
            let afterAtDrop = into.atDropInto(this, dragged, before);
            dragged = afterAtDrop.dragged;
            before = afterAtDrop.before;
            into = afterAtDrop.into;
        }

        // take any custom steps defined on the draggee
        if (dragged.atDropOnto) {
            let afterAtDrop = dragged.atDropOnto(this, before, into);
            dragged = afterAtDrop.dragged;
            before = afterAtDrop.before;
            into = afterAtDrop.into;
        }

        // actually insert
        into.insertBefore(dragged, before);
    }

    // this method is a delegate for atDrop, which may mutate
    // dragged, before, and into (the el called against)
    atDropIntoActor(self, dragged, before) {
        // in this context, 'this' is the dragged el,
        // and 'self' is the instance of this class
        let into = this;

        // always shrink elements dropped on this button,
        // but make them automatically grow again if dragged off
        dragged.classList.add("dropped-on-finder");
        dragged.atDrag = self.atDragFromFinder;

        // always drop after image in finder
        before = null;

        // returning both mutated and unmutated identities
        return {dragged, before, into};
    }

    // this method is a delegate for atDrop, which may mutate
    // dragged (the el called against), before, and into
    atDropJobLikeliesOnto(self, before, into) {
        // in this method's runtime context, 'this' means
        // the draggee, and 'self' is this class' instance
        let dragged = this;

        // when job likelies is dragged from its original L side location and then dropped,
        // it should place make an empty clone of itself at its origin, for later uses
        let empty = dragged.cloneNode(false);  // false leaves the clone empty

        // if this job-likelies was not dragged from the L side, but elsewhere,
        // no copy should be made or placed where this draggee just was
        let origin = dragged.parentElement;

        if (origin.id != "JobThings") {
            // returning mutated and unmutated identities
            return {dragged, before, into};
        }

        // copied el must be draggable and droppable,
        // with all the same handling as the original
        self.makeElementDragAndDrop(empty);
        empty.setAttribute("job-likelies", "job-likelies");
        empty.atDropOnto = dragged.atDropOnto;
        empty.atDropInto = dragged.atDropInto

        // repoint to the empty in the dom class
        self.dom.jobLikelies = empty;

        // insert the copy before the original,
        // to easily swap it into place
        let parent = dragged.parentElement;
        parent.insertBefore(empty, dragged);

        // the original node needs a new unique ID;
        // here, this means IndexController instance
        dragged.id = String(self.guid++);

        // returning mutated and unmutated identities
        return {dragged, before, into}
    }

    atDropIntoJobLikelies(self, dragged, before) {
        // in this context, 'this' is the drop-zone el,
        // and 'self' is the instance of this class
        let into = this;

        // if the dragged element is the old likelies, based on an
        // el attribute, swap it back for the current job likelies

        // region branch path: if what was dragged isn't the old likelies

        // if not the old likelies, determined using a custom
        // el attribute, don't do anything custom here
        if (!dragged.getAttribute("job-likelies")) {
            return {dragged, before, into};
        }
        // endregion branch path

        // region main path: swap in the draggee for the old job likelies

        // inserting the dragged element before the old likelies'
        // next sibling (which may be null) is the easiest way

        // getting the old likelies and copying its id to the new one
        let likelies = self.dom.jobLikelies;
        dragged.id = likelies.id;

        // getting old likelies' relative insertion-point identities
        into = likelies.parentElement;
        before = likelies.nextElementSibling;

        // dropping the old likelies; new one is inserted by caller
        into.removeChild(likelies);

        // swapping dom identity for other site handling
        self.dom.jobLikelies = dragged;

        // endregion main path

        // these returned values are used to perform the swap-in
        return {dragged, before, into};
    }

    whenTrashOvered(e) {
        e.preventDefault();
    }

    whenTrashed(e) {
        e.preventDefault();

        let id = e.dataTransfer.getData("text/plain");
        let dragged = this.dom.getElementById(id);
        let parent = this.dom.getParentByChildId(id);

        // make a copy of the draggee in the trash zone
        // and apply being-trashed styling to it;
        // for unknown reasons, if added without a delay,
        // .faded doesn't transition, so it is delayed
        let trashed = this.appendBubbleClone(dragged.innerText, this.trashed, this.dom.trasher);
        setTimeout(() => trashed.classList.add("faded"), 5);
        setTimeout(() => this.dom.trasher.removeChild(trashed), 500);

        // things from Alreadies are in the stored Graph,
        // so they're removed there and Alreadies is refreshed
        if (parent.id == this.dom.alreadies.id) {
            this.removeOnServer(dragged);
        }
        else {
            this.removeLocally(dragged, parent);
        }
    }

    async removeOnServer(dragged) {
        let factor = dragged.innerText;
        factor = {of: factor};

        await this.state.removeFromGraph([factor]);
        await this.fillAlreadies();
    }

    removeLocally(dragged, parent) {
        // removing the dragged el from the DOM only
        parent.removeChild(dragged);
    }

    // endregion Drag and drop, including custom delegates
}
