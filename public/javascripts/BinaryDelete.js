/**/

import {BinarySearch} from "./BinarySearch.js";

export class BinaryDelete {
    static delete(subject, topic) {
        let deleteAt = BinarySearch.searchExact(subject, topic);

        // possible edge case: topic isn't present,
        // so the input array is returned unchanged
        if (deleteAt == -1) {
            return subject;
        }

        // calling splice() with only two numbers mutates
        // the array, dropping the indicated elements
        subject.splice(deleteAt, 1);
        return subject;
    }
}
