/**/


// region external CJS dependencies

let express = require("express");
let router = express.Router();

// endregion external CJS dependencies


// region external ESM dependency TestServer

let service = null;

/* using an ESM module in CJS code with async use of import()
   for downstream use in either await or then() syntax */
async function loadService() {
    if (service != null) {
        return;
    }

    let holder = await import("../public/javascripts/TestServer.js");
    let definition = holder.TestServer;
    service = new definition();
}

// endregion external ESM dependency TestServer


// region routed testing-service methods

router.get("/start", (request, response, next) => {
    loadService()
        .then(x => {
            service.start();
        })
        .then(x => {
            response.send();
        });
});


router.get("/next", (request, response, next) => {
    let reply = service.next();
    response.send(reply);
});

// endregion routed testing-service methods


// region CJS self-export

module.exports = router;

// endregion CJS self-export

