/**/

let express = require("express");
let router = express.Router();

let server = null;

//* future: work out a by-user solution, or more naming options, or something *//
let defaultFileName = "Graph.json";

async function initServer() {
    if (server != null) {
        return;
    }

    /* awaiting the module, getting the class definition
       from it, and initing a new instance of that class */
    let module = await import("../public/javascripts/GraphServer.js");
    let definition = module.GraphServer;
    server = definition.fromFile(defaultFileName);
}

router.get("/start", (request, response, next) => {
    //* ?future: making the graph depend on the user *//
    initServer()
        .then(x => {
            response.send();
        });
});

router.get("/topEight", (request, response, next) => {
    initServer()
        .then(() => {
            let topEight = server.topEight();
            response.send(topEight);
        })
        .catch(() => {
            let empty = [];
            response.send(empty);
        })
    ;
});

router.get("/find/:factors", (request, response, next) => {
    /* future: finding by two or more criteria, possibly with
       a query string, addressed using urlencoded or similar */
    let factors = JSON.parse(request.params.factors);

    // actually finding; may work now, but probably only with one factor
    let found = server.find(factors);

    // returning the result as JSON
    response.send(found);
});

router.post("/add", (request, response, next) => {
    //* cruft: example data to expect: *//
    // let bundle = {
    //     "nodes": [
    //         {"vertex": "a", "edges": []},
    //         {"vertex": "b", "edges": ["a"]},
    //         {"vertex": "c", "edges": ["a", "b"]}
    //     ]
    // };

    initServer()
        .then(() => {
            let bundle = request.body.bundle;
            server.add(bundle);
            response.send();
        });
});

router.post("/remove", (request, response, next) => {
    initServer()
        .then(x => {
            let factors = request.body.factors;
            server.remove(factors);
            response.send();
        });
});

module.exports = router;
