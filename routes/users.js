const express = require("express");
const router = express.Router();

const bcrypt = require("bcrypt");

let server = null;

async function lazyInitServer() {
    // laziness
    if (server != null) {
        return;
    }

    // async stepping to the new instance
    let module = await import("../public/javascripts/UserServer.js");
    let definition = module.UserServer;
    server = new definition();

    // async initing the new instance, since initing does awaiting
    await server.init();
}

//* get users listing. *//
router.get("/", function (req, res, next) {
    res.send("respond with a resource");
});

//* looks at the current session to see if it has a user defined *//
router.get("/isUser", function (request, response, next) {
    let isUser = false;

    if (request.session.user) {
        isUser = true;
    }

    response.send(isUser);
});

//* looks at the store to see if the user is defined, and sets them
//  on the session if they are, and returns a result boolean *//
router.post("/authenticateUser", function (request, response, next) {
    let body = request.body;
    let identity = body.identity;
    let secret = body.secret;

    let user = null;

    isStoredUser(identity, secret)
        .then((x) => {
            if (x.isStored) {
                request.session.user = {identity};
                response.send(true);
            }
            else {
                response.send(false);
            }
        });
});

async function isStoredUser(identity, secret) {
    return { isStored: true, user: identity };

    /* &cruft, restore and reimplement with a new storage system, probably one or more files */
    // await lazyInitServer();
    // let user = await server.getUser(identity);
    //
    // if (!user) {
    //     return { isStored: false, user };
    // }
    //
    // let isStored = await bcrypt.compare(secret, user.secret);
    //
    // server.destroy();
    // server = null;
    //
    // return {isStored, user};
}

router.post("/leave", function (request, response, next) {
    request.session.destroy();
    response.send(true);
});

module.exports = router;
